"""praktikum URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path, include
from django.contrib import admin
from django.http import HttpResponse

# method view
def index(request):
	return HttpResponse("Hello world!")

def welcome(request,name):
	return HttpResponse("Welcome, " + name + "!")

# TODO Create a new path that redirects to the aboutme page like welcome page (Show your name and your Hobby)

def profilePage(request, name):
    return HttpResponse("nama saya : "+ name+ "\n hobi saya : bermain games")

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', index, name='index'),
    path('welcome/<str:name>', welcome, name='welcome'),
    path('profilePage/<str:name>', profilePage, name = 'Rafi'),
    # TODO Add a new path that redirects to the aboutme page 

]
